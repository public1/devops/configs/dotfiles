
syntax on
colorscheme default
set number

set autoindent " Indentation
set tabstop=4
set shiftwidth=4
set smarttab
set softtabstop=4

set mouse=a " Enable mouse click.
" set clipboard=unnamedplus " Use system clipboard.

" Autocompletion in command mode.
set wildmenu
set wildmode=longest:full,full
set wildoptions=pum
